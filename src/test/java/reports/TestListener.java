package reports;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    public void onTestStart(ITestResult result) {
        String testMethodName = result.getMethod().getMethodName();
        String testDescription = result.getMethod().getDescription();
        System.out.println();
        System.out.println("START TEST: " + testMethodName);
        if (testDescription != null) {
            if (!testDescription.isEmpty()) {
                System.out.println("Description: " + testDescription);
            }
        }
    }

    public void onTestSuccess(ITestResult tr) {
        System.out.println("TEST PASSED: " + tr.getMethod().getMethodName());
    }
    public void onTestFailure(ITestResult tr) {
        System.out.println("TEST Failed: " + tr.getMethod().getMethodName());
    }
//    onTestSkipped(ITestResult tr)
}
