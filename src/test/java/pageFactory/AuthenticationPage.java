package pageFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AuthenticationPage {
    WebDriver driver;

    @FindBy(how = How.ID, using = "input-login-username")
    //@FindBy(id = "input-login-username")
    private WebElement userField;

    @FindBy(how = How.ID, using = "input-login-password")
    private WebElement passField;

    @FindBy(how = How.ID, using = "login-submit")
    private WebElement submitButton;

    @FindBy(how = How.ID, using = "logout-submit")
    private WebElement logoutButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login_form\"]/div[2]/div/div")
    private WebElement userErrorField;

    @FindBy(how = How.XPATH, using = "//*[@id=\"login_form\"]/div[3]/div/div[1]")
    private WebElement passErrorField;

    @FindBy(how = How.ID, using = "login-error")
    private WebElement generalErrorField;

    public AuthenticationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void login(String username, String password) {
        userField.clear();
        userField.sendKeys(username);
        passField.clear();
        passField.sendKeys(password);
        submitButton.submit();
    }
    public void logOut() {
        logoutButton.click();
    }

    public boolean checkForError(String error, String type) {
        if (type.equals("userErr"))
            return error.equals(userErrorField.getText());
        else if (type.equals("passErr"))
            return error.equals(passErrorField.getText());
        else if (type.equals("generalErr"))
            return error.equals(generalErrorField.getText());

        return false;
    }

}
