package Utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.HashMap;
import java.util.Map;

public class SeleniumUtils {
    public static WebDriver getDriver(String browserType) {
        WebDriver driver = null;
        if (browserType.toUpperCase().equals(Browsers.CHROME.toString())) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("start-maximized");
            driver = new ChromeDriver(chromeOptions);

        } else if (browserType.toUpperCase().equals(Browsers.GALAXY_S5.toString())) {
            WebDriverManager.chromedriver().setup();
            ChromeOptions chromeOptions = new ChromeOptions();

            Map<String, Object> deviceMetrics = new HashMap<String, Object>();
            deviceMetrics.put("width", 360);
            deviceMetrics.put("height", 640);
            deviceMetrics.put("pixelRatio", 3.0);

            Map<String, Object> mobileEmulation = new HashMap<String, Object>();
            mobileEmulation.put("deviceMetrics", deviceMetrics);
            mobileEmulation.put("userAgent", "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");

            chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
            driver = new ChromeDriver(chromeOptions);

        } else if (browserType.toUpperCase().equals(Browsers.FIREFOX.toString())) {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browserType.toUpperCase().equals(Browsers.IE.toString())) {
            WebDriverManager.iedriver().setup();
            driver = new InternetExplorerDriver();
        } else if (driver == null) {
            System.out.println("WARNING the driver is null because the selection did not match an existing browser !!");
        }
        return driver;
    }

    public static WebDriver getDriver(Browsers browserType) {
        return getDriver(browserType.toString());
    }

}
