package tests;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsEqual.equalTo;

public class APITests {
    @Before
    public void setup() {
        RestAssured.baseURI = "https://restcountries.eu";
//        RestAssured.port = 443;
        RestAssured.useRelaxedHTTPSValidation();
    }

    @Test
    public void getInfoForCountry() {
        given().get("/rest/v2/name/{country}", "Romania").
                then().
                statusCode(200).
                assertThat()
                .body("name[0]", equalTo("Romania")).
                body("currencies[0].code[0]", equalTo("RON"));
    }

    @Test
    public void extractValue() {
        String capital = given().get("/rest/v2/name/{country}", "Romania").
                then().
                statusCode(200).
                extract().
                path("capital[0]");

        System.out.println("Capital of Romania is:" + capital);

        List<String> spelling = given().get("/rest/v2/name/{country}", "Romania")
                .then()
                .extract()
                .body()
                // here's the magic
                .jsonPath().getList("altSpellings[0]", String.class);

        System.out.println("Spelling elements");
        for (String spell : spelling) {
            System.out.println(spell);
        }
    }

    @Test
    public void verifyLanguages() {
        given().get("/rest/v2/name/{country}", "Romania")
                .then()
                .statusCode(200).
                assertThat()
                .body("languages[0].nativeName", hasItems("Română"));
    }

    @Test
    public void verifyBorders() {
        List<String> expectedBorders = Arrays.asList("BGR", "HUN", "MDA", "SRB", "UKR");
        List<String> borders = given().get("/rest/v2/name/{country}", "Romania")
                .then()
                .extract()
                .body()
                // here's the magic
                .jsonPath().getList("borders[0]", String.class);

        System.out.println("Borders elements");
        for (String border : borders) {
            System.out.println(border);
        }

        Assert.assertTrue(Arrays.deepEquals(borders.toArray(), expectedBorders.toArray()));
    }
}
