package tests;

import Utils.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class HoverTests extends BaseTest {
    Actions action;

    @BeforeTest
    public void setUp() {
        driver.get("http://" + hostname + "/stubs/hover.html#");
        action = new Actions(driver);
    }

    @DataProvider(name = "positiveData")
    public Iterator<Object[]> positiveData() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        // username, password, browser
        dp.add(new String[]{"item 1"});
        dp.add(new String[]{"item 2"});
        dp.add(new String[]{"item 3",});
        return dp.iterator();
    }

    @Test(dataProvider = "positiveData")
    public void mouseHoveTest(String item) {
        WebElement hoverButton = driver.findElement(By.className("dropbtn"));
        action.moveToElement(hoverButton).build().perform();

        WebElement item1 = driver.findElement(By.name(item));
        item1.click();
        System.out.println(item + " - Clicked");

        WebElement clickMe = Util.waitForGenericElement(driver, By.xpath("//p[contains(text(),'You clicked')]"), 10);
        Assert.assertTrue(clickMe.isDisplayed());
        Assert.assertEquals("You clicked " + item, clickMe.getText());
    }
}
