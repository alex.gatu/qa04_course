package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class FirefoxTests {

    @Test
    public void firefoxTest1() {
        WebDriver driver = new FirefoxDriver();
        System.setProperty("webdriver.gecko.driver",
                "C:\\Users\\Alex\\IdeaProjects\\qa04_course\\src\\test\\testResources\\geckodriver.exe");
        DesiredCapabilities cap = DesiredCapabilities.firefox();
        cap.setCapability("marionette", true);
        cap.setBrowserName("firefox");
        driver.get("https://www.google.com");
        // QUIT closes all browser instances
        driver.quit();
        // CLOSE closes just the current one
        //driver.close();
    }

    @Test
    public void firefoxTest2() {
        WebDriverManager.firefoxdriver().setup();
        WebDriver driver = new FirefoxDriver();

        driver.get("https://www.google.com");
        driver.quit();
    }
}
