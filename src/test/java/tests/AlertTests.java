//package tests;
//
//import org.openqa.selenium.Alert;
//import org.testng.Assert;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;
//import pageFactory.AlertsPage;
//
//public class AlertTests extends BaseTest {
//
//    @BeforeTest
//    public void setUp() {
//        driver.get("http://" + hostname + "/stubs/alert.html");
//    }
//
//    @Test
//    public void enterAndDismissAlert() {
//        String expectedResults = "Hello! I am an Javascript prompt dialog!\nYou cannot interact with the page unless you dismiss me";
//        AlertsPage alertsPage = new AlertsPage(driver);
//        alertsPage.promptDialogButton.click();
////        driver.findElement(By.id("prompt-trigger")).click();
//        Alert alert = driver.switchTo().alert();
//
//        System.out.println("Alert message:" + alert.getText());
//        Assert.assertEquals(alert.getText(), expectedResults);
//        alert.accept();
//    }
//
//}
