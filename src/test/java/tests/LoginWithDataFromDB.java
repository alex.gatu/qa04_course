package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.AuthenticationPage;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import Utils.Util;

import static Utils.SeleniumUtils.getDriver;

public class LoginWithDataFromDB {
    WebDriver driver;

    @DataProvider(name = "credentialsSQLDataProvider")
    public Iterator<Object[]> loginDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();

        try {
            System.out.println("Start Database connection");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/siit_aut?useSSL=false&serverTimezone=UTC", "root", "root");
            Statement stm = conn.createStatement();
//            ResultSet res = stm.executeQuery("Select * from students;");
            ResultSet res = stm.executeQuery("select * from users_credentials;");
            System.out.println("List users:");
            while (res.next()) {
                dp.add(new String[]{res.getString("username"), res.getString("password"), res.getString("browser")});
                System.out.println(res.getString("username") + " " + res.getString("password") + " " + res.getString("browser"));
            }
            res.close();
            conn.close();
        } catch (SQLException sexc) {
            System.out.println(sexc.getMessage());
            sexc.printStackTrace();
        }
        return dp.iterator();
    }

    @Test(dataProvider = "credentialsSQLDataProvider")
    private void loginSQLTest(String username, String password, String browser) {
        System.out.println("Start login test with:" + username + " " + password);
        driver = getDriver(browser);
        driver.get("http://86.125.50.44:8081/stubs/auth.html");
        AuthenticationPage loginPage = new AuthenticationPage(driver);
        //login
        loginPage.login(username, password);

        //validate that user was logged accordingly
        WebElement userNameElem = Util.waitForGenericElement(driver, By.id("user"), 10);
        Assert.assertEquals(username, userNameElem.getText());
        //click logout
        loginPage.logOut();
        driver.quit();
    }
}
