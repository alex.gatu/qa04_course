package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static Utils.SeleniumUtils.getDriver;

public class ParamsTest {
    WebDriver driver;

    @Parameters({"browserName"})
    @Test
    public void runByBrowserType(String browserName){
        driver = getDriver(browserName);
        driver.navigate().to("https://www.google.com/");
    }

    @AfterTest
    public void close(){
        driver.quit();
    }
}
