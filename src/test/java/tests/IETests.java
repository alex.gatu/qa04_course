package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class IETests {

    @Test
    public void ieTest01() {
        WebDriverManager.iedriver().setup();
        WebDriver driver = new InternetExplorerDriver();
        driver.get("https://www.google.com");
        driver.navigate().to("https://www.google.com");
        driver.quit();
        driver.close();
    }
}
