package tests;

import Utils.ExcelReader;
import Utils.Util;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.opencsv.CSVReader;
import dataModels.AccountModel;
import dataModels.LoginModel;
import dataModels.LoginXmlModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.AuthenticationPage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static Utils.Util.getListOfFiles;

public class LoginWithDataProvidersFromFile extends BaseTest {
    private String dataPath = "src/test/testResources/data";

    @DataProvider(name = "JSONDataProvider")
    public Iterator<Object[]> jsonDataProviderCollection() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Collection<Object[]> dp = new ArrayList<>();
        List<File> files = getListOfFiles(dataPath, "json");
        for (File f : files) {
            LoginModel[] modelList = mapper.readValue(f, LoginModel[].class);
            for (LoginModel m : modelList)
                dp.add(new Object[]{m});
        }
        return dp.iterator();
    }

    @DataProvider(name = "loginXMLDataProvider")
    public Iterator<Object[]> xmlDataProviderCollection() throws IOException {
        List<File> files = getListOfFiles(dataPath, "xml");

        Collection<Object[]> dp = new ArrayList<>();

        for (File f : files) {
            LoginXmlModel m = unMarshalLoginModel(f);
            dp.add(new Object[]{m});
        }
        return dp.iterator();
    }

    private LoginXmlModel unMarshalLoginModel(File f) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(LoginXmlModel.class);
            Unmarshaller jaxbUnMarshaller = jaxbContext.createUnmarshaller();
            return (LoginXmlModel) jaxbUnMarshaller.unmarshal(f);

        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DataProvider(name = "loginCSVDataProvider")
    public Object[][] csvDataProviderCollection() throws Exception {
        ClassLoader cl = getClass().getClassLoader();
        List<File> files = getListOfFiles(dataPath, "csv");
        File csvFile = files.get(0);

        Reader reader = Files.newBufferedReader(Paths.get(csvFile.getAbsolutePath()));
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> csvData = csvReader.readAll();

        Object[][] dp = new Object[csvData.size()][1];
        for (int i = 0; i < csvData.size(); i++) {
            AccountModel account = new AccountModel();
            account.setUsername(csvData.get(i)[0]);
            account.setPassword(csvData.get(i)[1]);

            LoginModel loginModel = new LoginModel();
            loginModel.setAccount(account);
            loginModel.setUserError(csvData.get(i)[2]);
            loginModel.setPasswordError(csvData.get(i)[3]);
            loginModel.setGeneralError(csvData.get(i)[4]);
            dp[i][0] = loginModel;
        }
        return dp;

    }

    @DataProvider(name = "ExcelDataProvider")
    public Object[][] excelDataProviderCollection() throws Exception {
        List<File> files = getListOfFiles(dataPath, "xlsx");

        Object[][] dp = null;
        for (File excelFile : files ) {

            String[][] excelData = ExcelReader.readExcelFile(excelFile, "users", true, true);
            dp = new Object[excelData.length][files.size()];

            for (int i = 0; i < dp.length; i++) {
                AccountModel account = new AccountModel();
                account.setUsername(excelData[i][0]);
                account.setPassword(excelData[i][1]);
                LoginModel loginModel = new LoginModel();
                loginModel.setAccount(account);
                loginModel.setUserError(excelData[i][2]);
                loginModel.setPasswordError(excelData[i][3]);
                loginModel.setGeneralError(excelData[i][4]);
                dp[i][0] = loginModel;
            }
        }
        return dp;

    }

    @Test(dataProvider = "loginCSVDataProvider")
    public void testLoginWithCSV(LoginModel loginModel) {
        runLogin(loginModel);
    }

    @Test(dataProvider = "ExcelDataProvider")
    public void testLoginWithExcel(LoginModel loginModel) {
        runLogin(loginModel);
    }

    @Test(dataProvider = "JSONDataProvider")
    public void testLoginWithJson(LoginModel loginModel) {
        runLogin(loginModel);
    }

    public void runLogin(LoginModel loginModel) {
        Boolean hassError = false;
        //setDriver(browser);
        driver.get("http://" + hostname + "/stubs/auth.html");
        AuthenticationPage authenticationPage = new AuthenticationPage(driver);
        // Inserting username and pass and click on submit
        authenticationPage.login(loginModel.getAccount().getUsername(), loginModel.getAccount().getPassword());

        if (loginModel.getPasswordError().length() > 0) {
            Assert.assertTrue(authenticationPage.checkForError(loginModel.getPasswordError(), "userErr"));
            hassError = true;
        }

        if (loginModel.getGeneralError().length() > 0) {
            Assert.assertTrue(authenticationPage.checkForError(loginModel.getGeneralError(), "generalErr"));
            hassError = true;
        }

        if (loginModel.getUserError().length() > 0) {
            Assert.assertTrue(authenticationPage.checkForError(loginModel.getUserError(), "userErr"));
            hassError = true;
        }

        if (!hassError) {
            //verify login page appear
            System.out.println("Has no error");
            WebElement userNameElem = waitForGenericElement(By.id("user"), 10);
            Assert.assertEquals(loginModel.getAccount().getUsername(), userNameElem.getText());
            //click logout
            authenticationPage.logOut();
        }
    }

    @Test(dataProvider = "loginXMLDataProvider")
    public void testLoginWithXML(LoginXmlModel loginModel) {
        Boolean hassError = false;
        //setDriver(browser);
        driver.get("http://" + hostname + "/stubs/auth.html");
        AuthenticationPage authenticationPage = new AuthenticationPage(driver);
        // Inserting username and pass and click on submit
        authenticationPage.login(loginModel.getAccount().getUsername(), loginModel.getAccount().getPassword());

        if (loginModel.getPasswordError().length() > 0) {
            Assert.assertTrue(authenticationPage.checkForError(loginModel.getPasswordError(), "userErr"));
            hassError = true;
        }

        if (loginModel.getGeneralError().length() > 0) {
            Assert.assertTrue(authenticationPage.checkForError(loginModel.getGeneralError(), "generalErr"));
            hassError = true;
        }

        if (loginModel.getUserError().length() > 0) {
            Assert.assertTrue(authenticationPage.checkForError(loginModel.getUserError(), "userErr"));
            hassError = true;
        }

        if (!hassError) {
            //verify login page appear
            System.out.println("Has no error");
            WebElement userNameElem = waitForGenericElement(By.id("user"), 10);
            Assert.assertEquals(loginModel.getAccount().getUsername(), userNameElem.getText());
            //click logout
            authenticationPage.logOut();
        }
    }

}
