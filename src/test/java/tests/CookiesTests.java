package tests;

import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Set;

public class CookiesTests extends BaseTest {

    @BeforeTest
    public void setUp() {
        driver.get("https://" + cookiesUrl);
    }

    @Test
    public void listCookies() {
        System.out.println("List Cookies:");
        Set<Cookie> listCookies = driver.manage().getCookies();

        for (Cookie cookie : listCookies) {
            System.out.println("Name:" + cookie.getName() + " value: " + cookie.getValue());
        }
    }

    @Test
    public void createNamedCookie() {
        String cookieName = "myCookie";
        Cookie myCookie = new Cookie(cookieName, "Testing");
        driver.manage().addCookie(myCookie);

//        get cookie
        System.out.println("Verify current cookie" + driver.manage().getCookieNamed(cookieName).getName());
        Assert.assertEquals(myCookie, driver.manage().getCookieNamed(cookieName));

//        delete cookie
        driver.manage().deleteCookieNamed(cookieName);

        Assert.assertNull(driver.manage().getCookieNamed(cookieName));
    }
}
