package dataModels;

import dataModels.AccountModel;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LoginXmlModel {
    private AccountModel account;
    private String userError;
    private String passwordError;
    private String generalError;

    public String getUserError() {
        return userError;
    }

    @XmlElement
    public void setUserError(String userError) {
        this.userError = userError;
    }

    public String getPasswordError() {
        return userError;
    }

    @XmlElement
    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

    public String getGeneralError() {
        return generalError;
    }

    @XmlElement
    public void setGeneralError(String generalError) {
        this.generalError = generalError;
    }

    public AccountModel getAccount() {
        return account;
    }

    @XmlElement
    public void setAccount(AccountModel accountModel) {
        this.account = accountModel;
    }

}