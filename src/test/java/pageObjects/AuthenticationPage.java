package pageObjects;

import Utils.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AuthenticationPage {
    WebDriver driver;
    Util utilWait;
    By userFieldBy = By.id("input-login-username");
    By passFieldBy = By.id("input-login-password");
    By submitButtonBy = By.id("login-submit");
    By logoutButtonBy = By.id("logout-submit");
    By userErrorFieldBy = By.xpath("//*[@id=\"login_form\"]/div[2]/div/div");
    By passErrorFieldBy = By.xpath("//*[@id=\"login_form\"]/div[3]/div/div[1]");
    By generalErrorFieldBy = By.id("login-error");

    private WebElement userField;
    private WebElement passField;
    private WebElement submitButton;
    private WebElement logoutButton;
    private WebElement userErrorField;
    private WebElement passErrorField;
    private WebElement generalErrorField;

    public AuthenticationPage(WebDriver driver) {
        this.driver = driver;
    }

    public void setUserField() {
        this.userField = Util.waitForGenericElement(driver, userFieldBy, 10);
    }

    public void setPassField() {
        this.passField = Util.waitForGenericElement(driver, passFieldBy, 10);
    }

    public void setSubmitButton() {
        this.submitButton = Util.waitForGenericElement(driver, submitButtonBy, 10);
    }

    public void setLogoutButton() {
        this.logoutButton = Util.waitForGenericElement(driver, logoutButtonBy, 10);
    }

    public void setUserErrorField() {
        this.userErrorField = Util.waitForGenericElement(driver, userErrorFieldBy, 10);
    }

    public void setPassErrorField() {
        this.passErrorField = Util.waitForGenericElement(driver, passErrorFieldBy, 10);
    }

    public void setGeneralErrorField() {
        this.generalErrorField = Util.waitForGenericElement(driver, generalErrorFieldBy, 10);
    }

    public void login(String username, String password) {
        this.setPassField();
        this.setUserField();
        this.setSubmitButton();
        userField.clear();
        userField.sendKeys(username);
        passField.clear();
        passField.sendKeys(password);
        submitButton.submit();
    }

    public void logOut() {
        setLogoutButton();
        logoutButton.click();
    }

    public boolean checkForError(String error, String type) {
        setUserErrorField();
        setPassErrorField();
        setGeneralErrorField();

        if (type.equals("userErr"))
            return error.equals(userErrorField.getText());
        else if (type.equals("passErr"))
            return error.equals(passErrorField.getText());
        else if (type.equals("generalErr"))
            return error.equals(generalErrorField.getText());

        return false;
    }
}
