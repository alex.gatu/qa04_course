package course1;

public class Draw {

    public static void main(String[] args) {
        draw(5); // un patrat cu latura 5
        draw(23f); // sfera cu raza 23
        draw(3,5); // dreptunghi cu l = 3 si L =5
    }

    // patrat
    public static void draw(int latura) {
        System.out.println("Patrat cu latura " + latura);
        System.out.println("Aria: " + (latura * latura));
    }

    // cerc
    public static void draw(float raza) {
        System.out.println("Cerc cu raza " + raza);
        System.out.println("Aria: " + Constants.PI * raza * raza);
    }

    // dreptunghi
    public static void draw(int lungime, int latime) {
        System.out.println("Dreptunghi cu lunigmea " + lungime + " si latimea " + latime);
        System.out.println("Aria: " + lungime * latime);
    }

}
