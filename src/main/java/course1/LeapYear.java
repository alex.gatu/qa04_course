package course1;

public class LeapYear {

    public static void getDaysLeapYear(int startYear, int endYear) {
        for (int i = startYear ; i <= endYear ; i++) {
            int days = 28;
            if (i % 4 == 0) {
                if (i % 100 != 0) {
                    days = 29;
                }
                else {
                    if (i % 400 == 0) {
                        days = 29;
                    }
                }
            }
            System.out.println("Anul " + i + " are " + days + " zile!");
        }
    }

    public static int getDaysForYear(int year) {
        int days = 28;
        if (year % 4 == 0) {
            if (year % 100 != 0) {
                return 29;
            }
            else {
                if (year % 400 == 0) {
                    return 29;
                }
            }
        }
        return 28;
    }

    public static void getDaysLeapYear2(int startYear, int endYear) {
        for (int i = startYear ; i <= endYear ; i++) {
            System.out.println(getDaysForYear(i));
        }
    }

    public static void main(String[] args) {
        getDaysLeapYear(1900, 2016);
        getDaysLeapYear2(100, 1500);
    }

}
