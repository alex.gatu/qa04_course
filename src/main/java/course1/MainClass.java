package course1;

public class MainClass {
    public static void main(String[] args) {
        Circle myCircle = new Circle();
        myCircle.setRadius(2);
        //myCircle.radius = 2;
        System.out.println(myCircle.getArea());

        Person myPerson = new Person();
        myPerson.eat();
        myPerson.increaseAge();

        Light light = new Light();
        light.turnOff();
        light.turnOn();
        light.dim(2);
        light.brighten(3);
        light.turnOff();
        // static access
        Utils.splitString("Ana are mere");
        // non static access via the object
        Utils u = new Utils();
        u.splitStringNonStatic("Ana are mere");

    }
}
