package course1;

public class Square {
    // este vizibila doar in clasa
    private double side; // default is 0

    // vizibil in packet si in subclase
    protected void setSide(double side) {
        if (side < 0 ){
            System.out.println("Lautura trebuie sa fie >= 0");
        }
        else {
            this.side = side;
        }
    }

    // vizibil oriunde
    public double getArea () {
        return side * side;
    }

    // este vizibila doar in clasa
    private void doSomething() {

    }

    public Square() {
        //doSomething();
    }

    public Square(double side) {
        this.side = side;
    }
}
