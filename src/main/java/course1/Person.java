package course1;

class Person {
        String name;
        byte age; // default is 0 ....
        Boolean hungry;

        void eat(){
                System.out.println("This pearson is eating ....");
                hungry = false;
        }

        void increaseAge() {
                System.out.println("This person has " + age + " years");
                age++; // age = age + 1;
                System.out.println("This pearson now has " + age + " years");

        }
}
