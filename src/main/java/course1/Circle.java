package course1;

public class Circle {

    double radius;

    void setRadius(double rad) {
        radius =  rad;
    }

    double getArea() {
        return Constants.PI * radius * radius;
    }
}
